/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018;

import java.nio.ByteBuffer;

import org.tahomarobotics.sim.messaging.MessageHandler;
import org.tahomarobotics.sim.messaging.MessageSocketClient;
import org.tahomarobotics.sim.model.ModelIF;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.offseason2018.message.ResetCommand;
import org.tahomarobotics.sim.model.offseason2018.message.StatusMessage;
import org.tahomarobotics.sim.model.offseason2018.message.TorqueCommand;

public class UnityModel implements ModelIF {

	public final static String HOST = "localhost";
	public final static int PORT = 4444;
	
	private final TorqueCommand torqueCommand = new TorqueCommand();
	private final ResetCommand resetCommand = new ResetCommand();
	private final StatusMessage statusMessage = new StatusMessage();
	
	private final MessageSocketClient messageClient = new MessageSocketClient(HOST, PORT);

	private final DriveModel drive = new DriveModel() {
		@Override
		protected void updateDynamics(double dT) {
			setDriveTorque(leftTorque, rightTorque);
		}
	};
	
	private final LiftModel lift = new LiftModel() {
		@Override
		protected void updateDynamics(double dT) {
			setLiftForce(force);
		}
	};
	
	private final ArmModel  arm  = new ArmModel() {
		@Override
		protected void updateDynamics(double dT) {
			setArmTorque(torque);
		}
	};
	
	public UnityModel() {
		
		messageClient.registerHandler(StatusMessage.MESSAGE_ID, new MessageHandler<StatusMessage>() {
		
			@Override
			public void onMessage(ByteBuffer buffer) {
				
				statusMessage.deserialize(buffer);
				
				drive.x = statusMessage.x;
				drive.y = statusMessage.y;
				drive.heading = statusMessage.heading;
				
				drive.fwdVelocity = statusMessage.fwdVelocity;
				drive.rotVelocity = statusMessage.rotVelocity;
				
				drive.leftPosition = statusMessage.leftDrivePosition;
				drive.leftVelocity = statusMessage.leftDriveVelocity;
				
				drive.rightPosition = statusMessage.rightDrivePosition;
				drive.rightVelocity = statusMessage.rightDriveVelocity;
				
				lift.position = statusMessage.liftPosition;
				lift.velocity = statusMessage.liftVelocity;

				arm.position = statusMessage.armPosition;
				arm.velocity = statusMessage.armVelocity;
				
				//System.out.format("Status: %f %f\n", arm.position, arm.velocity);;
			}			
		});
	}
	
	@Override
	public void initialize(SimRobot sim) {
		drive.initialize(sim);
		lift.initialize(sim);
		arm.initialize(sim);		
	}


	
	@Override
	public boolean isConnected() {
		return messageClient.isConnected();
	}
	
	private boolean changed = false;
	
	private boolean isChanged() {
		return changed;
	}
	
	@Override
	public void resetPosition(double x, double y, double heading) {
		if (isConnected()) {
			resetCommand.x = x;
			resetCommand.y = y;
			resetCommand.heading = heading;
			messageClient.sendMessage(resetCommand);
		}
	}

	private void setDriveTorque(double left, double right) {
		if (torqueCommand.leftTorque != left || torqueCommand.rightTorque != right) {
			changed = true;
			torqueCommand.leftTorque = left;
			torqueCommand.rightTorque = right;
		}
	}

	private void setLiftForce(double force) {
		if (torqueCommand.liftForce != force) {
			changed = true;
			torqueCommand.liftForce = force;
		}
	}
	
	private void setArmTorque(double torque) {
		if (torqueCommand.armTorque != torque) {
			changed = true;
			torqueCommand.armTorque = torque;
			//System.out.println("arm torque="+torque);
		}
	}

	@Override
	public void update(double time, double dT) {
		
		if (isConnected()) {
			
			drive.update(dT);
			lift.update(dT);
			arm.update(dT);
			
			if (isChanged()) {
				messageClient.sendMessage(torqueCommand);
				changed = false;
				 //System.out.println("Sending " + torqueCommand);	
			}
		}
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		if (torqueCommand.enabled != enabled) {
			changed = true;
			torqueCommand.enabled = enabled;
		}
	}

	@Override
	public void getPose(double[] pose) {
		pose[0] = drive.getX();
		pose[1] = drive.getY();
		pose[2] = drive.getHeading();	
	}

	@Override
	public String getName() {
		return "2018 Ursa Origin - Unity";
	}
}
