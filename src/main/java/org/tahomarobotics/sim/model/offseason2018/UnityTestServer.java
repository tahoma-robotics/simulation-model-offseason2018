/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018;

import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;

import org.tahomarobotics.sim.messaging.MessageHandler;
import org.tahomarobotics.sim.messaging.MessageSocketServer;
import org.tahomarobotics.sim.model.offseason2018.message.ResetCommand;
import org.tahomarobotics.sim.model.offseason2018.message.StatusMessage;
import org.tahomarobotics.sim.model.offseason2018.message.TorqueCommand;

public class UnityTestServer extends InternalModel {

	private final long DEFAULT_PERIOD = 20;

	private final TorqueCommand torqueCommand = new TorqueCommand();
	private final ResetCommand resetCommand = new ResetCommand();
	private final StatusMessage statusMessage = new StatusMessage();

	private final MessageSocketServer messageServer = new MessageSocketServer(UnityModel.HOST, UnityModel.PORT);

	private final Object sync = new Object();

	public UnityTestServer() {

		messageServer.registerHandler(TorqueCommand.MESSAGE_ID, new MessageHandler<TorqueCommand>() {

			@Override
			public void onMessage(ByteBuffer buffer) {

				torqueCommand.deserialize(buffer);
				System.out.println("Receiving " + torqueCommand);

				synchronized (sync) {
					setEnabled(torqueCommand.enabled);
					drive.leftTorque = torqueCommand.leftTorque;
					drive.rightTorque = torqueCommand.rightTorque;
					lift.force = torqueCommand.liftForce;
					arm.torque = torqueCommand.armTorque;
				}
			}
		});

		messageServer.registerHandler(ResetCommand.MESSAGE_ID, new MessageHandler<ResetCommand>() {

			@Override
			public void onMessage(ByteBuffer buffer) {

				resetCommand.deserialize(buffer);
				synchronized (sync) {
					resetPosition(resetCommand.x, resetCommand.y, resetCommand.heading);
				}
			}
		});

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			private double time = System.currentTimeMillis() * 0.001;

			@Override
			public void run() {

				double time = System.currentTimeMillis() * 0.001;
				double dT = time - this.time;
				this.time = time;

				if (messageServer.isConnected()) {

					synchronized (sync) {
						updateDynamics(time, dT);

						statusMessage.x = drive.x;
						statusMessage.y = drive.y;
						statusMessage.heading = drive.heading;
						statusMessage.fwdVelocity = drive.fwdVelocity;
						statusMessage.rotVelocity = drive.rotVelocity;
						statusMessage.leftDrivePosition = drive.leftPosition;
						statusMessage.leftDriveVelocity = drive.leftVelocity;
						statusMessage.rightDrivePosition = drive.rightPosition;
						statusMessage.rightDriveVelocity = drive.rightVelocity;

						statusMessage.liftPosition = lift.position;
						statusMessage.liftVelocity = lift.velocity;

						statusMessage.armPosition = arm.position;
						statusMessage.armVelocity = arm.velocity;

						messageServer.sendMessage(statusMessage);
					}
				}
			}

		}, 0, DEFAULT_PERIOD);

	}

	public static void main(String args[]) {
		new UnityTestServer();
	}
}
