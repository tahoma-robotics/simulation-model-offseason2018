/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018;

import org.tahomarobotics.sim.model.ModelProperties;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Transmission;

public class ArmModel {

	private static final double ARM_ANGLE_MIN = Math.toRadians(0.0);
	private static final double ARM_ANGLE_MAX = Math.toRadians(155);
	private static final double ARM_COG_OFFSET = Math.toRadians(-24.38); // from horizontal
	public static final double ARM_MASS = 9.674 * 0.453592;
	private static final double ARM_CG = 11.675 * 0.0254;
	private static final double ARM_MOMENT_OF_INERTIA = ARM_MASS * ARM_CG * ARM_CG;
	private static final double ARM_MAX_GRAVITY_TORQUE = ARM_MASS * ARM_CG * -ModelProperties.GRAVITATIONAL_ACCELERATION;

	protected double torque;
	protected double position;
	protected double velocity;

	// Arm motor and transmission
	private final Motor motor = Motor.create775Pro();
	private static final double ARM_INITIAL_STAGE = 100.0 / 12.0;
	private static final double ARM_2ND_STAGE = 60.0 / 20.0;
	private static final double ARM_3RD_STAGE = 60.0 / 20.0;
	private static final double ARM_FINAL_STAGE = 56.0 / 16.0;

	private static final double ARM_GEAR_RATIO = ARM_INITIAL_STAGE * ARM_2ND_STAGE * ARM_3RD_STAGE * ARM_FINAL_STAGE;

	private final Transmission drive = new Transmission(new Motor[] {motor}, true, ARM_GEAR_RATIO, ARM_GEAR_RATIO);
	
	public ArmModel() {
	}

	public void initialize(SimRobot sim) {
		sim.registerMotor(SimRobotMap.ARM_MOTOR, motor);
		sim.registerTransmission(SimRobotMap.ARM_MOTOR, drive);
		torque = 0;
		position = 0;
		velocity = 0;
	}
	
	public void update(double dT) {
		
		// update the drive model
		torque = updateDriveTorque(position, velocity);
		//System.out.println("arm torque="+torque);

		// update the dynamics
		updateDynamics(dT);
	}
	
	protected double updateDriveTorque(double position, double velocity) {
		drive.setVelocity(velocity);
		drive.setPosition(position);
		drive.update();
		return drive.getTorque();
	}

	protected void updateDynamics(double dT) {
		double gravityTorque = ARM_MAX_GRAVITY_TORQUE * Math.cos(position + ARM_COG_OFFSET);
		double armAcceleration = (torque + gravityTorque) / ARM_MOMENT_OF_INERTIA;
		velocity += armAcceleration * dT;
		position += velocity * dT;	

		// limit position to hard stops
		if (position < ARM_ANGLE_MIN) {
			position = ARM_ANGLE_MIN;
			velocity = 0;
		} else if (position > ARM_ANGLE_MAX) {
			position = ARM_ANGLE_MAX;
			velocity = 0;
		}
	}
}
