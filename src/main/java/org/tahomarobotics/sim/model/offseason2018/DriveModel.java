/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018;

import java.util.ArrayList;
import java.util.List;

import org.tahomarobotics.sim.model.IMU;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Solenoid;
import org.tahomarobotics.sim.model.Transmission;
import org.tahomarobotics.sim.model.Transmission.Shift;

@SuppressWarnings("unused")
public class DriveModel {

	//private static final Logger LOGGER = LoggerFactory.getLogger(DriveModel.class);


	private static final double KILOGRAM_PER_LBMASS = 0.453592;
	private static final double METERS_PER_INCH = 0.0254;
	private static final double MASS = 127.9 * KILOGRAM_PER_LBMASS; // kg
	private static final double INTERIA = 20779.439  * KILOGRAM_PER_LBMASS * METERS_PER_INCH * METERS_PER_INCH; // lbmass in^2 to kg m^2
	private static final double WHEEL_DIAMETER = 5.970 * METERS_PER_INCH;
	private static final double WHEEL_RADIUS = WHEEL_DIAMETER / 2;
	private static final double TRACK_HALF_WIDTH = 25.0 / 2.0 * METERS_PER_INCH;
	private static final double WHEEL_SPACING = 12.0 * METERS_PER_INCH;
	private static final double Crr = 0.004; // rolling resistance
	private static final double GRAVITATIONAL_ACCELERATION =  9.80655;
	private static final double WEIGHT = MASS * GRAVITATIONAL_ACCELERATION;

	private static final double FIELD_WIDTH = 324.0 * METERS_PER_INCH;
	private static final double FIELD_LENGHT = 648.0 * METERS_PER_INCH;

	// Drive motors and transmission
	private final Motor leftRearMotor = Motor.createMiniCIM();
	private final Motor leftMiddleMotor = Motor.createMiniCIM();
	private final Motor leftFrontMotor = Motor.createMiniCIM();
	private final Motor rightRearMotor = Motor.createMiniCIM();
	private final Motor rightMiddleMotor = Motor.createMiniCIM();
	private final Motor rightFrontMotor = Motor.createMiniCIM();

	private static final double INITIAL_STAGE = 54.0 / 20.0;
	private static final double HIGH_MID_STAGE = 34.0 / 54.0;
	private static final double LOW_MID_STAGE = 44.0 / 44.0;
	private static final double FINAL_STAGE = 60.0 / 30.0;

	private static final double HIGH_GEAR_RATIO = INITIAL_STAGE * HIGH_MID_STAGE * FINAL_STAGE;
	private static final double LOW_GEAR_RATIO = INITIAL_STAGE * LOW_MID_STAGE * FINAL_STAGE;

	private final Transmission leftDrive = new Transmission(new Motor[] { leftRearMotor, leftMiddleMotor, leftFrontMotor }, 
			true, HIGH_GEAR_RATIO, LOW_GEAR_RATIO);
	private final Transmission rightDrive = new Transmission(new Motor[] { rightRearMotor, rightMiddleMotor, rightFrontMotor },
			false, HIGH_GEAR_RATIO, LOW_GEAR_RATIO);

	private final Solenoid solenoid = new Solenoid();

	private final IMU imu = new IMU();
	
	protected double leftTorque;
	protected double leftPosition;
	protected double leftVelocity;
	
	protected double rightTorque;
	protected double rightPosition;
	protected double rightVelocity;
	
	protected double fwdVelocity;
	protected double rotVelocity;
	protected double x;
	protected double y;
	protected double heading;
	
	private final LimitMovement fieldPerimeter = new LimitMovement(0 + TRACK_HALF_WIDTH, 0 + TRACK_HALF_WIDTH, 
			FIELD_LENGHT - TRACK_HALF_WIDTH, FIELD_WIDTH - TRACK_HALF_WIDTH, true, "Perimeter");
	
	private final LimitMovement scale = new LimitMovement(313.25 * METERS_PER_INCH - TRACK_HALF_WIDTH, 97.279 * METERS_PER_INCH - TRACK_HALF_WIDTH, 
			334.75 * METERS_PER_INCH + TRACK_HALF_WIDTH, 226.721 * METERS_PER_INCH + TRACK_HALF_WIDTH, false, "Scale");
	
	private final LimitMovement switchBlue = new LimitMovement(140 * METERS_PER_INCH - TRACK_HALF_WIDTH, 85.25 * METERS_PER_INCH - TRACK_HALF_WIDTH, 
			196 * METERS_PER_INCH + TRACK_HALF_WIDTH, 238.75 * METERS_PER_INCH + TRACK_HALF_WIDTH, false, "Switch-Blue");
	
	private final LimitMovement switchRed = new LimitMovement(452 * METERS_PER_INCH - TRACK_HALF_WIDTH, 85.25 * METERS_PER_INCH - TRACK_HALF_WIDTH, 
			508.188 * METERS_PER_INCH + TRACK_HALF_WIDTH, 238.75 * METERS_PER_INCH + TRACK_HALF_WIDTH, false, "Switch-Red");
	
	private final List<LimitMovement> limits = new ArrayList<>();
	
	protected DriveModel() {
	}
	
	public void initialize(SimRobot sim) {
		sim.registerMotor(SimRobotMap.LEFT_REAR_MOTOR, leftRearMotor);
		sim.registerMotor(SimRobotMap.LEFT_MIDDLE_MOTOR, leftMiddleMotor);
		sim.registerMotor(SimRobotMap.LEFT_FRONT_MOTOR, leftFrontMotor);
		sim.registerMotor(SimRobotMap.RIGHT_REAR_MOTOR, rightRearMotor);
		sim.registerMotor(SimRobotMap.RIGHT_MIDDLE_MOTOR, rightMiddleMotor);
		sim.registerMotor(SimRobotMap.RIGHT_FRONT_MOTOR, rightFrontMotor);
		sim.registerTransmission(SimRobotMap.LEFT_REAR_MOTOR, leftDrive);
		sim.registerTransmission(SimRobotMap.RIGHT_REAR_MOTOR, rightDrive);
		sim.registerSolenoid(SimRobotMap.PCM_MODULE, SimRobotMap.DRIVE_SOLENOID, solenoid);
		sim.registerImu(SimRobotMap.PIGEON_IMU, imu);
		
		leftTorque = leftPosition = leftVelocity = 0;		
		rightTorque = rightPosition = rightVelocity = 0;	
		fwdVelocity = rotVelocity = 0;
		x = y = heading = 0;
		
		limits.add(fieldPerimeter);
		limits.add(scale);
		limits.add(switchBlue);
		limits.add(switchRed);
	}
	
	public void update(double dT) {

		// update the drive model
		leftTorque = updateDriveTorque(leftDrive, leftPosition, leftVelocity);
		rightTorque = updateDriveTorque(rightDrive, rightPosition, rightVelocity);
		
		imu.setZRotationalVelocity(rotVelocity);
		
		// update the dynamics
		updateDynamics(dT);
	}
	
	protected double updateDriveTorque(Transmission drive, double position, double velocity) {
		drive.shift(solenoid.isExtended() ? Shift.HIGH : Shift.LOW);
		drive.setVelocity(velocity);
		drive.setPosition(position);
		drive.update();
		return drive.getTorque();
	}

	public void resetPosition(double x, double y, double heading) {
		this.x = x;
		this.y = y;
		this.heading = heading;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getHeading() {
		return heading;
	}

	private static final double CG_OFFSET =  1.0 * METERS_PER_INCH; // forward of center
	private static final double CG_TO_CENTER = CG_OFFSET;
	private static final double CG_TO_FRONT = WHEEL_SPACING - CG_OFFSET;
	
	private static final double STATIC_FRICTION = 1.0;
	private static final double KINETIC_FRICTION = 0.8;
	private static final double KINETIC_THRESHOLD = Math.toRadians(1.0); // 1 deg/sec
	private static final double frontWeight = WEIGHT  * CG_OFFSET / WHEEL_SPACING;
	private static final double centerWeight = WEIGHT - frontWeight;
	private static final double centerRadius = Math.sqrt(TRACK_HALF_WIDTH * TRACK_HALF_WIDTH + CG_TO_CENTER * CG_TO_CENTER);
	private static final double frontRadius = Math.sqrt(TRACK_HALF_WIDTH * TRACK_HALF_WIDTH + CG_TO_FRONT * CG_TO_FRONT);	
	private static final double centerFrictionAngle = Math.atan(CG_TO_CENTER/TRACK_HALF_WIDTH);
	private static final double frontFrictionAngle = Math.atan(CG_TO_FRONT/TRACK_HALF_WIDTH);
	private static final double frictionTorque = centerWeight * centerRadius * Math.sin(centerFrictionAngle) + frontWeight * frontRadius * Math.sin(frontFrictionAngle);
	private static final double staticFrictionTorque = frictionTorque * STATIC_FRICTION;
	private static final double kineticFrictionTorque = frictionTorque * KINETIC_FRICTION;
	private static final double SCRUB_DAMPING = 10;
	private static final double MAX_SCRUB = 5.0;
	
	private double getScrubTorque(double appliedTorque, double rotVelocity) {
		double scrubTorque = Math.min(Math.abs(rotVelocity) * SCRUB_DAMPING, MAX_SCRUB);
		scrubTorque =  Math.signum(rotVelocity) * scrubTorque;
		return scrubTorque;
	}
	
	protected void updateDynamics(double dT) {
		
		double leftForce = leftTorque / WHEEL_RADIUS;
		double rightForce = rightTorque / WHEEL_RADIUS;
		
		double fwdForce = rightForce + leftForce; // N = kg m/s^2
		double rotTorque = (rightForce - leftForce) * TRACK_HALF_WIDTH; // Nm

		double rollingResistance = Math.signum(fwdVelocity) * Crr * MASS * GRAVITATIONAL_ACCELERATION;
		
		double fwdAccel = (fwdForce - rollingResistance) / MASS; // N / kg = kg m/s^2 / kg = m/s^2
		double rotAccel = (rotTorque - getScrubTorque(rotTorque, rotVelocity)) / INTERIA;  // Nm / kg m^2 = kg m/s^2 m / kg / m^2 = 1 / s^2 = rad/s^2
		
		fwdVelocity += fwdAccel * dT;
		rotVelocity += rotAccel * dT;
		double deltaHeading = rotVelocity * dT;
		double averageHeading = heading + deltaHeading / 2;
		heading += deltaHeading;
		
		double fwdDeltaDistance = fwdVelocity * dT;
		double newX = x + fwdDeltaDistance * Math.cos(averageHeading);
		double newY = y + fwdDeltaDistance * Math.sin(averageHeading);
		
		
		// limit movement based walls
		for (LimitMovement limit : limits) {
			if (limit.isLimited(newX, newY)) {
				System.out.println(limit);
				newX = x;
				newY = y;
				fwdVelocity = 0;
			}
		}
		x = newX;
		y = newY;
				
		double deltaVelocity = rotVelocity * TRACK_HALF_WIDTH;
		double leftVelocity  = fwdVelocity - deltaVelocity;
		double rightVelocity = fwdVelocity + deltaVelocity;
		
		this.leftVelocity = leftVelocity / WHEEL_RADIUS;
		this.rightVelocity = rightVelocity / WHEEL_RADIUS;

	}

}
