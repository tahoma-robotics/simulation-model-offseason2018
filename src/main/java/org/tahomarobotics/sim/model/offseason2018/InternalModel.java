/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018;

import org.tahomarobotics.sim.model.ModelIF;
import org.tahomarobotics.sim.model.SimRobot;

public class InternalModel implements ModelIF {

	protected final DriveModel drive = new DriveModel();
	protected final LiftModel lift = new LiftModel();
	protected final ArmModel  arm  = new ArmModel();

	@Override
	public void initialize(SimRobot sim) {
		drive.initialize(sim);
		lift.initialize(sim);
		arm.initialize(sim);
	}

	@Override
	public boolean isConnected() {
		return true;
	}

	@Override
	public void setEnabled(boolean enabled) {
	}
	
	@Override
	public void resetPosition(double x, double y, double heading) {
		drive.resetPosition(x, y, heading);
	}

	@Override
	public void update(double time, double dT) {
		drive.update(dT);
		lift.update(dT);
		arm.update(dT);
	}
	
	public void updateDynamics(double time, double dT) {
		drive.updateDynamics(dT);
		lift.updateDynamics(dT);
		arm.updateDynamics(dT);
	}


	@Override
	public void getPose(double[] pose) {
		pose[0] = drive.getX();
		pose[1] = drive.getY();
		pose[2] = drive.getHeading();
		
	}

	@Override
	public String getName() {
		return "2018 Ursa Origin";
	}
}
