/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.sim.model.ModelProperties;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Solenoid;
import org.tahomarobotics.sim.model.Transmission;
import org.tahomarobotics.sim.model.Transmission.Shift;

public class LiftModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(LiftModel.class);

	private static final double POSITION_MIN = 0;
	private static final double POSITION_MAX = 62.5 * 0.0254;
	
	private static final double LIFT_SPROCKET_RADIUS = 36 * 0.25 * 0.0254 / ( 2 * Math.PI );
	private static final double DISTANCE_PER_ANGLE = 2 * LIFT_SPROCKET_RADIUS; // 2x due to double stage
	private static final double LIFT_1ST_STAGE_MASS = 8.222 * 0.453592;
	private static final double LIFT_2ND_STAGE_MASS = 5.562 * 0.453592;
	private static final double LIFT_MASS = LIFT_1ST_STAGE_MASS + LIFT_2ND_STAGE_MASS + ArmModel.ARM_MASS;
	
	private static final double F_GRAVITY = LIFT_MASS * ModelProperties.GRAVITATIONAL_ACCELERATION;
	private static final double INTERIA = LIFT_1ST_STAGE_MASS / 2 + LIFT_2ND_STAGE_MASS + ArmModel.ARM_MASS;
	
	// Lift motors and transmission
	private final Motor topMotor = Motor.create775Pro();
	private final Motor midMotor = Motor.create775Pro();
	private final Motor botMotor = Motor.create775Pro();
	
	private static final double LIFT_INITIAL_STAGE = 85.0 / 12.0;
	private static final double LIFT_2ND_STAGE = 85.0 / 20.0;
	private static final double LIFT_3RD_STAGE = 54.0 / 24.0;
	private static final double LIFT_HIGH_MID_STAGE = 24.0 / 54.0;
	private static final double LIFT_LOW_MID_STAGE = 44.0 / 34.0;
	private static final double LIFT_FINAL_STAGE = 56.0 / 56.0;

	private static final double LIFT_HIGH_GEAR_RATIO = LIFT_INITIAL_STAGE * LIFT_2ND_STAGE * LIFT_3RD_STAGE * LIFT_HIGH_MID_STAGE * LIFT_FINAL_STAGE;
	private static final double LIFT_LOW_GEAR_RATIO  = LIFT_INITIAL_STAGE * LIFT_2ND_STAGE * LIFT_3RD_STAGE * LIFT_LOW_MID_STAGE  * LIFT_FINAL_STAGE;

	private final Transmission drive = new Transmission(new Motor[] {topMotor, midMotor, botMotor}, 
			false, LIFT_HIGH_GEAR_RATIO, LIFT_LOW_GEAR_RATIO);
	
	private final Solenoid solenoid = new Solenoid();
	
	protected double force;
	protected double position;
	protected double velocity;
	
	private boolean liftTransmissionInHighGear = true;

	public LiftModel() {
	}
	
	public void initialize(SimRobot sim) {
		sim.registerMotor(SimRobotMap.LIFT_TOP_MOTOR,	 topMotor);
		sim.registerMotor(SimRobotMap.LIFT_MIDDLE_MOTOR, midMotor);
		sim.registerMotor(SimRobotMap.LIFT_BOTTOM_MOTOR, botMotor);
		sim.registerTransmission(SimRobotMap.LIFT_TOP_MOTOR, drive);
		sim.registerSolenoid(SimRobotMap.PCM_MODULE, SimRobotMap.LIFT_SOLENOID, solenoid);
		force = 0;
		position = 0;
		velocity = 0;
	}

	public void update(double dT) {

		// update gear
		boolean state = !solenoid.isExtended();		
		if (liftTransmissionInHighGear != state) {
			LOGGER.info("liftTransmissionInHighGear: " + (state ? "ON" : "OFF"));
			liftTransmissionInHighGear = state;
		}
		drive.shift(state ? Shift.HIGH : Shift.LOW);
		
		
		// update the drive model
		force = updateDriveForce(position, velocity);
		
		// update the dynamics
		updateDynamics(dT);
	}
	
	protected double updateDriveForce(double position, double velocity) {
		// set drive velocity (rad/sec) from linear velocity (m/s)
		drive.setVelocity(velocity / DISTANCE_PER_ANGLE);
		drive.setPosition(position / DISTANCE_PER_ANGLE);
		drive.update();
		return drive.getTorque() / LIFT_SPROCKET_RADIUS;
	}

	protected void updateDynamics(double dT) {
		double acceleration = (force - F_GRAVITY) / INTERIA;
		velocity += acceleration * dT;
		position += velocity * dT;	
		//System.out.println("Lift Force " + force + " p: " + position + " v:" + velocity);
		// limit position to hard stops
		if (position < POSITION_MIN) {
			position = POSITION_MIN;
			velocity = 0;
		} else if (position > POSITION_MAX) {
			position = POSITION_MAX;
			velocity = 0;
		}
	}
}
