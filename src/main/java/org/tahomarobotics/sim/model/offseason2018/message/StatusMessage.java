/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018.message;

import java.nio.ByteBuffer;

import org.tahomarobotics.sim.messaging.Message;

public class StatusMessage  implements Message {

	public static final int MESSAGE_ID = 100;
	
	public double x;
	public double y;
	public double heading;
	public double fwdVelocity;
	public double rotVelocity;
	public double leftDrivePosition;
	public double leftDriveVelocity;
	public double rightDrivePosition;
	public double rightDriveVelocity;
	public double liftPosition;
	public double liftVelocity;
	public double armPosition;
	public double armVelocity;
	
	@Override
	public void serialize(ByteBuffer buffer) {
		buffer.putDouble(x);
		buffer.putDouble(y);
		buffer.putDouble(heading);
		buffer.putDouble(fwdVelocity);
		buffer.putDouble(rotVelocity);
		buffer.putDouble(leftDrivePosition);
		buffer.putDouble(leftDriveVelocity);
		buffer.putDouble(rightDrivePosition);
		buffer.putDouble(rightDriveVelocity);
		buffer.putDouble(liftPosition);
		buffer.putDouble(liftVelocity);
		buffer.putDouble(armPosition);
		buffer.putDouble(armVelocity);
	}

	@Override
	public void deserialize(ByteBuffer buffer) {
		x = buffer.getDouble();
		y = buffer.getDouble();
		heading = buffer.getDouble();
		fwdVelocity = buffer.getDouble();
		rotVelocity = buffer.getDouble();
		leftDrivePosition = buffer.getDouble();
		leftDriveVelocity = buffer.getDouble();
		rightDrivePosition = buffer.getDouble();
		rightDriveVelocity = buffer.getDouble();
		liftPosition = buffer.getDouble();
		liftVelocity = buffer.getDouble();
		armPosition = buffer.getDouble();
		armVelocity = buffer.getDouble();
	}

	@Override
	public int getMessageSize() {
		return 13 * Double.SIZE / Byte.SIZE;
	}

	@Override
	public int getMessageId() {
		return MESSAGE_ID;
	}
	
	@Override
	public String toString() {
		return String.format("%s %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f", 
				this.getClass().getSimpleName(),  
				x, y, heading, fwdVelocity, rotVelocity, leftDriveVelocity, rightDriveVelocity);
	}

}
