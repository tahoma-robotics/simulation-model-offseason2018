/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.sim.model.offseason2018.message;

import java.nio.ByteBuffer;

import org.tahomarobotics.sim.messaging.Message;

public class TorqueCommand implements Message {
	
	public static final int MESSAGE_ID = 101;
	
	public double leftTorque;
	public double rightTorque;
	public boolean enabled;

	public double liftForce;

	public double armTorque;
	
	@Override
	public void serialize(ByteBuffer buffer) {
		buffer.putDouble(leftTorque);
		buffer.putDouble(rightTorque);
		buffer.putDouble(liftForce);
		buffer.putDouble(armTorque);
		buffer.put((byte)(enabled ? 1 : 0));
	}

	@Override
	public void deserialize(ByteBuffer buffer) {
		leftTorque = buffer.getDouble();
		rightTorque = buffer.getDouble();
		liftForce = buffer.getDouble();
		armTorque = buffer.getDouble();
		enabled = buffer.get() == (byte)1;
	}
	
	@Override
	public int getMessageId() {
		return MESSAGE_ID;
	}

	@Override
	public int getMessageSize() {
		return (4 * Double.SIZE + Byte.SIZE) / Byte.SIZE;
	}

	@Override
	public String toString() {
		return String.format("%s %s %6.3f %6.3f %6.3f %6.3f", 
				this.getClass().getSimpleName(), enabled ? "Enabled" : "Disabled", 
						leftTorque, rightTorque, liftForce, armTorque);
	}

	

}
